package com.gft.digitalbank.exchange.solution;

import javax.jms.Connection;
import javax.jms.JMSException;
import javax.jms.MessageConsumer;
import javax.jms.Session;
import org.apache.commons.logging.Log;


public class JmsUtil {
	
	public static void close(Session session, Log log) {
		if (session != null) {
			try {
				session.close();
			} catch (JMSException ex) {
				log.warn("Cannot close JMS session", ex);
			}
		}
	}
	
	public static void close(MessageConsumer consumer, Log log) {
		if (consumer != null) {
			try {
				consumer.close();
			} catch (JMSException ex) {
				log.warn("Cannot close JMS message consumer", ex);
			}
		}
	}
	
	public static void stop(Connection jmsConnection, Log log) {
		if (jmsConnection != null) {
			try {
				jmsConnection.stop();
			} catch (JMSException ex) {
				log.warn("Cannot stop JMS connection", ex);
			}
		}
	}
	
	public static void close(Connection jmsConnection, Log log) {
		if (jmsConnection != null) {
			try {
				jmsConnection.close();
			} catch (JMSException ex) {
				log.warn("Cannot stop JMS connection", ex);
			}
		}
	}	
}
