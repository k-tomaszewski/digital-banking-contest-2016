package com.gft.digitalbank.exchange.solution;

import com.gft.digitalbank.exchange.Exchange;
import com.gft.digitalbank.exchange.listener.ProcessingListener;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.function.Supplier;
import javax.jms.Connection;
import javax.jms.JMSException;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

/**
 * Your solution must implement the {@link Exchange} interface.
 */
public class StockExchange implements Exchange {

    private static final Log LOG = LogFactory.getLog(StockExchange.class);
	
	// static to be shared accross subsequent tests
    private static final Supplier<Connection> jmsConnectionSupplier = new JmsConnectionSupplier();
	private static final ExecutorService executorService = Executors.newCachedThreadPool();
	
	private final List<Future<?>> receiverFutures = new ArrayList<>();

    private ProcessingListener processingListener;
    private List<String> destinations;


    public StockExchange() {
        LOG.info(">>>> CREATED");
    }

    @Override
    public void register(ProcessingListener processingListener) {
        this.processingListener = processingListener;
    }

    @Override
    public void setDestinations(List<String> list) {
        destinations = new ArrayList<>(list);
        LOG.info("Destinations: " + destinations);
    }

    @Override
    public void start() {
        if (destinations == null) {
            LOG.info(">>>> NO DESTINATIONS GIVEN");
            return;
        }

        final long startMillis = System.currentTimeMillis();
        Connection jmsConnection = null;
        try {
            jmsConnection = jmsConnectionSupplier.get();

            for (String destination : destinations) {
                BrokerMessageReceiver msgReceiver = new BrokerMessageReceiver(jmsConnection, destination);				
				receiverFutures.add(executorService.submit(msgReceiver));
            }
			
            jmsConnection.start();
            LOG.info(String.format(">>>> STARTED in %.2f s\n", (System.currentTimeMillis() - startMillis) / 1000.0));
			/////////////////
			
			// TODO
			waitForMessageReceivers(receiverFutures);

			/////////////////
            LOG.info(String.format(">>>> COMPLETED in %.2f s\n", (System.currentTimeMillis() - startMillis) / 1000.0));
			
			processingListener.processingDone(null);
			
        } catch (RuntimeException e) {
            LOG.error(">>>> FAILED: " + e.getMessage(), e);
            throw e;
			
        } catch (JMSException ex) {
            LOG.error(">>>> FAILED (JMS): " + ex.getMessage(), ex);
            throw new RuntimeException("JMS error", ex);
			
        } catch (InterruptedException ex) {
			LOG.error(">>>> INTERRUPTED", ex);
			
		} finally {
			receiverFutures.forEach(future -> future.cancel(true));
			//receiverFutures.clear();
			
			//JmsUtil.stop(jmsConnection, LOG);
			//JmsUtil.close(jmsConnection, LOG);
        }
    }
	
	static void waitForMessageReceivers(List<Future<?>> receiverFutures) throws InterruptedException {
		for (Future<?> receiverFuture : receiverFutures) {
			try {
				receiverFuture.get();	// wait
			} catch (ExecutionException ex) {
				LOG.warn("Execution error in broker message receiver", ex);
				throw new RuntimeException("Execution error in broker message receiver", ex);
			}
		}
	}
}
