package com.gft.digitalbank.exchange.solution;

import java.util.function.Supplier;
import javax.jms.Connection;
import javax.jms.ConnectionFactory;
import javax.jms.ConnectionMetaData;
import javax.jms.ExceptionListener;
import javax.jms.JMSException;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;


public class JmsConnectionSupplier implements Supplier<Connection>, ExceptionListener {
    
    private static final Log LOG = LogFactory.getLog(JmsConnectionSupplier.class);
    
    private Connection theConnection;

    
    @Override
    public Connection get() {
        if (theConnection == null) {
            try {
                Context context = new InitialContext();
                ConnectionFactory connectionFactory = (ConnectionFactory) context.lookup("ConnectionFactory");
                LOG.info("Obtained JMS connection factory: " + connectionFactory.getClass().getName());
                
                Connection connection = connectionFactory.createConnection();
                connection.setClientID("kt-stock-exchange");
                connection.setExceptionListener(this);
                
                ConnectionMetaData metaData = connection.getMetaData();
                LOG.info(String.format("Created JMS connection: %s ver. %s", metaData.getJMSProviderName(), metaData.getJMSVersion()));
                theConnection = connection;
				
				theConnection.start();
				theConnection.stop();
                
            } catch (NamingException | JMSException e) {
                LOG.error("Cannot obtain JMS connection", e);
                throw new RuntimeException("Cannot obtain JMS connection", e);
            }
        } else {
            LOG.info("Reusing existing JMS connection");
        }
        return theConnection;
    }

    @Override
    public void onException(JMSException jmse) {
        LOG.error("JMS connection problem reported by the provider: " + jmse.getMessage(), jmse);
    }
}
