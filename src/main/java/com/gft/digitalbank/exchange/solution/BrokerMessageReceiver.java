package com.gft.digitalbank.exchange.solution;

import javax.jms.Connection;
import javax.jms.JMSException;
import javax.jms.MessageConsumer;
import javax.jms.Session;
import javax.jms.TextMessage;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;


public class BrokerMessageReceiver implements Runnable {
	
	public static final String MESSAGE_TYPE_PROP = "messageType";
	private static final Log LOG = LogFactory.getLog(BrokerMessageReceiver.class);
	
	final Connection jmsConnection;
	final String queueName;
	
	Session jmsSession;
	MessageConsumer jmsMessageConsumer;

	
	public BrokerMessageReceiver(Connection jmsConnection, String queueName) {
		this.jmsConnection = jmsConnection;
		this.queueName = queueName;
	}

	@Override
	public void run() {
		try {
			jmsSession = jmsConnection.createSession(false, Session.AUTO_ACKNOWLEDGE);
			jmsMessageConsumer = jmsSession.createConsumer(jmsSession.createQueue(queueName));
			
			for (;;) {
				LOG.info(">> WAITING for message from " + queueName);
				TextMessage msg = (TextMessage) jmsMessageConsumer.receive();
				
				final String messageType = msg.getStringProperty(MESSAGE_TYPE_PROP);
				LOG.info(">> RECEIVED message-type: " + messageType);
				
				if ("SHUTDOWN_NOTIFICATION".equals(messageType)) {
					LOG.info(">>>> shutdown message received on " + queueName);
					break;				
				}
			}			
		} catch (JMSException ex) {
			LOG.error("Error during broker message reception on " + queueName, ex);
		} finally {
			closeResources();
		}
		LOG.info(">>>> Broker message receiver FINISHED for " + queueName);
	}
    
	private void closeResources() {
		JmsUtil.close(jmsMessageConsumer, LOG);
		jmsMessageConsumer = null;
		JmsUtil.close(jmsSession, LOG);
		jmsSession = null;
	}
}
