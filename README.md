# My solution for Digital Banking 2016 contest

## Technology
1. Java 8
1. Maven, pom.xml for JAR packaged application
1. JMS
1. multi-threading with use of ExecutorService and Feature from java.util.concurrent package

## Contest
Please refer to https://bitbucket.org/digitalbanking/contest2016/overview for original source of Digital Banking 2016 contest.

If the above is not available here is a screenshot with the contest description:
![contest description](https://bytebucket.org/k-tomaszewski/digital-banking-contest-2016/raw/41c6f0b5c12c76b7267f96f2665c32fb6a39e765/doc/Screenshot_digitalbanking-contest-2016.png)
